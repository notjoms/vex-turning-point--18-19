@echo off
echo ------------------------------------
echo Ready to upload?
pause
echo ------------------------------------
echo Building
echo ------------------------------------
make
echo ------------------------------------
echo Uploading
echo ------------------------------------
prosv5 upload --target v5 --name "8332A Blue Program" --slot 2 --run-after ./bin/output.bin
echo ------------------------------------
echo Done
echo ------------------------------------
pause