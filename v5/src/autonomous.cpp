#include "main.h"
using namespace okapi;
const int LEFT_BACK_DT_PORT = 2;
const int LEFT_FRONT_DT_PORT = 5;
const int LEFT_FLY_PORT = 6;
const int INTAKE_PORT = 10;
const int FLIPPER_PORT = 11;
const int RIGHT_FLY_PORT = 14;
const int VISION_PORT = 16;
const int RIGHT_FRONT_DT_PORT = 17;
const char ULTRASONIC_OUT_PORT = 'G';
const char ULTRASONIC_IN_PORT = 'H';
const int RIGHT_BACK_DT_PORT = 20;

const int SIG_RED = 2;
const int SIG_BLUE = 3;

using namespace okapi::literals;
extern int autonSelection;
okapi::Motor flipperauton(FLIPPER_PORT, false, AbstractMotor::gearset::red, AbstractMotor::encoderUnits::degrees);
/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */

 okapi::MotorGroup left_dt_group_auton({okapi::Motor(LEFT_FRONT_DT_PORT, false, okapi::AbstractMotor::gearset::green, okapi::AbstractMotor::encoderUnits::degrees), okapi::Motor(LEFT_BACK_DT_PORT, false, okapi::AbstractMotor::gearset::green, okapi::AbstractMotor::encoderUnits::degrees)});
 okapi::MotorGroup right_dt_group_auton({okapi::Motor(RIGHT_FRONT_DT_PORT, false, okapi::AbstractMotor::gearset::green, okapi::AbstractMotor::encoderUnits::degrees), okapi::Motor(RIGHT_BACK_DT_PORT, false, okapi::AbstractMotor::gearset::green, okapi::AbstractMotor::encoderUnits::degrees)});
 auto drivetrainAuton = okapi::ChassisControllerFactory::create(
 								left_dt_group_auton, // left side
 								right_dt_group_auton, // right side
 								okapi::AbstractMotor::gearset::green, // gearset
 								{4.0_in, 9.5_in} // diameter of wheels and width of drivetrain
 								);
okapi::Motor intakeAuton(INTAKE_PORT);
okapi::MotorGroup fly_auton(
								{okapi::Motor
									         (
																	LEFT_FLY_PORT,
																	false,
																	AbstractMotor::gearset::blue,
																	AbstractMotor::encoderUnits::degrees
									         ),
								okapi::Motor
									         (
																	RIGHT_FLY_PORT,
																	true,
																	AbstractMotor::gearset::blue,
																	AbstractMotor::encoderUnits::degrees
									         )
								}
);
const float radius_of_wheels_in = 2.0f;
const float m_PI = 3.141592653589793238463393f;
void intakeUp(double msec){
  intakeAuton.moveVelocity(200);
  delay(msec);
  intakeAuton.moveVelocity(0);
}
void intakeDown(double msec){
  intakeAuton.moveVelocity(-200);
  delay(msec);
  intakeAuton.moveVelocity(0);
}
int inchesToTicksO(int inches){
	return 360*inches/(2*PI*2);
}
/*void intakeUntilPrimed(){
  while(ultrasonic.get_value()>7){
    intake.moveVelocity(200);
  }
  intake.moveVelocity(0);
}*/
void moveDV(int distance, float velocity){
drivetrainAuton.setMaxVelocity(velocity);
while(okapi::Motor(LEFT_FRONT_DT_PORT).getPosition()<inchesToTicksO(distance)/2){
drivetrainAuton.forward(velocity);
}
drivetrainAuton.forward(0);
}
void autonomous() {
	using namespace okapi::literals;
	right_dt_group_auton.setReversed(true);
	drivetrainAuton.setBrakeMode(AbstractMotor::brakeMode::coast);
  /*struct Output {
    double kP, kI, kD;
  };*/

	switch (autonSelection) {
		case 0://red 1
      flipperauton.setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
      drivetrainAuton.setMaxVelocity(200);
      drivetrainAuton.moveDistance(44_in);
      intakeUp(800);
      intakeDown(125);
      fly_auton.moveVelocity(3000/5);
      drivetrainAuton.moveDistance(-45_in);
      drivetrainAuton.forward(.5);
      delay(150);
      drivetrainAuton.stop();
      drivetrainAuton.setMaxVelocity(100);
      drivetrainAuton.turnAngle(-87.5_deg);
      drivetrainAuton.setMaxVelocity(110);
      drivetrainAuton.moveDistance(10_in);
      intakeUp(200);
      drivetrainAuton.moveDistance(19_in);
      intakeUp(500);
      fly_auton.moveVelocity(0);
      drivetrainAuton.turnAngle(-10_deg);
      drivetrainAuton.moveDistance(18_in);
      delay(100);
      drivetrainAuton.moveDistance(-25_in);
      drivetrainAuton.turnAngle(90_deg);
      drivetrainAuton.setMaxVelocity(160);
      intakeAuton.moveVelocity(-180);
      drivetrainAuton.moveDistance(26_in);
      intakeAuton.moveVelocity(0);
      break;
		case 1://red 2
      flipperauton.setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
      drivetrainAuton.setMaxVelocity(200);
      drivetrainAuton.moveDistance(44_in);
      intakeUp(800);
      intakeDown(125);
      drivetrainAuton.moveDistance(-22_in);
      drivetrainAuton.turnAngle(-90_deg);
      drivetrainAuton.forward(1);
      delay(3000);
      drivetrainAuton.stop();
			break;
		case 2://blue 1
      flipperauton.setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
      drivetrainAuton.setMaxVelocity(200);
      drivetrainAuton.moveDistance(44_in);
      intakeUp(800);
      intakeDown(125);
      drivetrainAuton.turnAngle(7_deg);
      fly_auton.moveVelocity(3000/5);
      drivetrainAuton.moveDistance(-45_in);
      drivetrainAuton.forward(.5);
      delay(150);
      drivetrainAuton.stop();
      drivetrainAuton.setMaxVelocity(100);
      drivetrainAuton.turnAngle(97.5_deg);
      drivetrainAuton.setMaxVelocity(100);
      drivetrainAuton.moveDistance(10_in);
      intakeUp(200);
      drivetrainAuton.moveDistance(19_in);
      intakeUp(500);
      fly_auton.moveVelocity(0);
      drivetrainAuton.turnAngle(10_deg);
      drivetrainAuton.moveDistance(18_in);
      delay(100);
      drivetrainAuton.moveDistance(-25_in);
      drivetrainAuton.turnAngle(-90_deg);
      drivetrainAuton.setMaxVelocity(160);
      intakeAuton.moveVelocity(-180);
      drivetrainAuton.moveDistance(26_in);
      intakeAuton.moveVelocity(0);
      break;
		case 3://blue 2
      flipperauton.setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
      drivetrainAuton.setMaxVelocity(200);
      drivetrainAuton.moveDistance(44_in);
      intakeUp(800);
      intakeDown(125);
      drivetrainAuton.moveDistance(-22_in);
      drivetrainAuton.turnAngle(90_deg);
      drivetrainAuton.forward(1);
      delay(3000);
      drivetrainAuton.stop();
			break;
		default:

			break;
	}
}
