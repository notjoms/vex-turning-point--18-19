#include "main.h"
const int LEFT_BACK_DT_PORT = 2;
const int LEFT_FRONT_DT_PORT = 5;
const int LEFT_FLY_PORT = 6;
const int INTAKE_PORT = 10;
const int FLIPPER_PORT = 11;
const int RIGHT_FLY_PORT = 14;
const int VISION_PORT = 16;
const int RIGHT_FRONT_DT_PORT = 17;
const char ULTRASONIC_OUT_PORT = 'G';
const char ULTRASONIC_IN_PORT = 'H';
const int RIGHT_BACK_DT_PORT = 20;

const int SIG_RED = 2;
const int SIG_BLUE = 3;
int autonSelection = 2;
/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */

void initialize() {
  //pros::Vision vision_sensor (VISION_PORT, pros::E_VISION_ZERO_CENTER);
  pros::ADIUltrasonic ultrasonic (ULTRASONIC_OUT_PORT, ULTRASONIC_IN_PORT);
}

/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {}

/**
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
static lv_res_t btnm_action(lv_obj_t * btnm, const char *txt){
  if(strcmp(txt, "Red 1")) autonSelection = 0;
  else if(strcmp(txt, "Red 2")) autonSelection = 1;
  else if(strcmp(txt, "Blue 1")) autonSelection = 2;
  else if(strcmp(txt, "Blue 2")) autonSelection = 3;
  else autonSelection = 2;
  return LV_RES_OK;
}
void competition_initialize() {
  lv_obj_t * scr = lv_obj_create(NULL, NULL);
  lv_scr_load(scr);
  lv_obj_t * title = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(title, "Auton Selection");
  lv_obj_align(title, scr, LV_ALIGN_IN_TOP_LEFT, 0, 0);
  //
  static const char * btnm_map[] = {"Red 1", "Red 2", "Blue 1", "Blue 2", ""};
  lv_obj_t * btnm1 = lv_btnm_create(lv_scr_act(), NULL);
  lv_btnm_set_map(btnm1, btnm_map);
  lv_btnm_set_action(btnm1, btnm_action);
  lv_obj_set_size(btnm1, LV_HOR_RES, LV_VER_RES / 2);
}
