#include "main.h"
#define LEFT_BACK_DT_PORT 2
#define LEFT_FRONT_DT_PORT 5
#define LEFT_FLY_PORT 6
#define INTAKE_PORT 10
#define FLIPPER_PORT 11
#define RIGHT_FLY_PORT 14
#define VISION_PORT 16
#define RIGHT_FRONT_DT_PORT 17
#define ULTRASONIC_OUT_PORT 'G'
#define ULTRASONIC_IN_PORT 'H'
#define RIGHT_BACK_DT_PORT 20
#define SIG_RED 2
#define SIG_BLUE 3
//creating buffers for the motor strings
char velLeftString[64];
char velRightString[64];
char posLeftString[64];
char posRightString[64];
char torqueLeftString[64];
char torqueRightString[64];
char batteryCapacityString[64];
char batteryCurrentString[64];
char batteryTempString[64];
char batteryVoltageString[64];
//different pages
int screenPage = 0;
bool isRed = true;
char* pageTitles[4] = {
	"DT Front Info",
	"DT Back Info",
	"Flywheel Info",
	"Flipper and Intake Info"
};

//creating graphics objects on screen
lv_obj_t * scr = lv_obj_create(NULL, NULL);
lv_obj_t * toggleButton = lv_btn_create(scr, NULL);
lv_obj_t * toggleButtonLabel = lv_label_create(toggleButton, NULL);
lv_obj_t * velLeft = lv_label_create(scr, NULL);
lv_obj_t * velRight = lv_label_create(scr, NULL);
lv_obj_t * posLeft = lv_label_create(scr, NULL);
lv_obj_t * posRight = lv_label_create(scr, NULL);
lv_obj_t * torqueLeft = lv_label_create(scr, NULL);
lv_obj_t * torqueRight = lv_label_create(scr, NULL);

lv_obj_t * batteryHolder = lv_cont_create(scr, NULL);

lv_obj_t * batteryCapacityLabel = lv_label_create(batteryHolder, NULL);
lv_obj_t * batteryCurrentLabel = lv_label_create(batteryHolder, NULL);
lv_obj_t * batteryTempLabel = lv_label_create(batteryHolder, NULL);
lv_obj_t * batteryVoltageLabel = lv_label_create(batteryHolder, NULL);
//declaring all the meter objects
lv_obj_t * lineMeterHolderLeft;
lv_obj_t * lineMeter1;
lv_obj_t * lineMeterLabel1;
lv_obj_t * lineMeter2;
lv_obj_t * lineMeterLabel2;
lv_obj_t * lineMeter3;
lv_obj_t * lineMeterLabel3;
lv_obj_t * lineMeterHolderRight;
lv_obj_t * lineMeter4;
lv_obj_t * lineMeterLabel4;
lv_obj_t * lineMeter5;
lv_obj_t * lineMeterLabel5;
lv_obj_t * lineMeter6;
lv_obj_t * lineMeterLabel6;
//setting up motors

okapi::MotorGroup left_dt_group({2,5});
okapi::MotorGroup right_dt_group({17,20});
auto drivetrain = okapi::ChassisControllerFactory::create(
								left_dt_group, // left side
								right_dt_group, // right side
								okapi::AbstractMotor::gearset::green, // gearset
								{4.0_in, 9.75_in} // diameter of wheels and length of drivetrain
								);
okapi::MotorGroup fly_group(
								{okapi::Motor
									(
																	LEFT_FLY_PORT,
																	false,
																	AbstractMotor::gearset::blue,
																	AbstractMotor::encoderUnits::degrees
									),
									okapi::Motor
									(
																	RIGHT_FLY_PORT,
																	true,
																	AbstractMotor::gearset::blue,
																	AbstractMotor::encoderUnits::degrees
									)
								}
								);
//fly_group.setVelPID(0.0f, 1.0f, 0.001f, 0.1f);
okapi::Motor flipper(FLIPPER_PORT, false, AbstractMotor::gearset::red, AbstractMotor::encoderUnits::degrees);
okapi::Motor intake(INTAKE_PORT);
pros::Vision vision_sensor (VISION_PORT, pros::E_VISION_ZERO_CENTER);
pros::ADIUltrasonic ultrasonic (ULTRASONIC_OUT_PORT, ULTRASONIC_IN_PORT);
//button press task
static lv_res_t btn_click_action(lv_obj_t * btn){
								uint8_t id = lv_obj_get_free_num(btn);
								//do things
								screenPage++;
								if(screenPage>3) screenPage = 0;
								//
								return LV_RES_OK;
}
lv_obj_t * ultrasoniclabel = lv_label_create(scr, NULL);
char ultLabel[64];
bool primed = false;
//self-explanatory
void screenInit(){
								//load screen canvas
								//lv_obj_set_size(scr, 480, 272);
								//create center button and assign actions
								lv_label_set_text(toggleButtonLabel, "DT Info");
								lv_cont_set_fit(toggleButton, true, true);
								lv_obj_align(toggleButton, NULL, LV_ALIGN_IN_TOP_MID, 0, 20);
								lv_obj_set_free_num(toggleButton, 1);
								lv_btn_set_action(toggleButton, LV_BTN_ACTION_CLICK, btn_click_action);
								//

								//initialize the left meter set
								lineMeterHolderLeft = lv_cont_create(scr, NULL);
								lv_obj_set_size(lineMeterHolderLeft, 125, 250);
								//
								lineMeter1 = lv_lmeter_create(lineMeterHolderLeft, NULL);
								lv_obj_set_size(lineMeter1, 80, 80);
								lineMeterLabel1 = lv_label_create(lineMeter1, NULL);
								lv_label_set_text(lineMeterLabel1, "LML1");
								lv_obj_align(lineMeterLabel1, lineMeter1, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
								lv_lmeter_set_range(lineMeter1, 0, 600);
								//
								lineMeter2 = lv_lmeter_create(lineMeterHolderLeft, NULL);
								lv_obj_set_size(lineMeter2, 80, 80);
								lineMeterLabel2 = lv_label_create(lineMeter2, NULL);
								lv_label_set_text(lineMeterLabel2, "LML2");
								lv_obj_align(lineMeterLabel2, lineMeter2, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
								lv_lmeter_set_range(lineMeter2, 0, 360);
								//
								lineMeter3 = lv_lmeter_create(lineMeterHolderLeft, NULL);
								lv_obj_set_size(lineMeter3, 80, 80);
								lineMeterLabel3 = lv_label_create(lineMeter3, NULL);
								lv_label_set_text(lineMeterLabel3, "LML3");
								lv_obj_align(lineMeterLabel3, lineMeter3, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
								lv_lmeter_set_range(lineMeter3, 0, 11);
								//
								lv_obj_align(lineMeter1, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);
								lv_obj_align(lineMeter2, NULL, LV_ALIGN_CENTER, 0, 0);
								lv_obj_align(lineMeter3, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);
								//

								//initialize the right meter set
								lineMeterHolderRight = lv_cont_create(scr, NULL);
								lv_obj_set_size(lineMeterHolderRight, 125, 250);
								//
								lineMeter4 = lv_lmeter_create(lineMeterHolderRight, NULL);
								lv_obj_set_size(lineMeter4, 80, 80);
								lineMeterLabel4 = lv_label_create(lineMeter4, NULL);
								lv_label_set_text(lineMeterLabel4, "LML4");
								lv_obj_align(lineMeterLabel4, lineMeter4, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
								lv_lmeter_set_range(lineMeter4, 0, 600);
								//
								lineMeter5 = lv_lmeter_create(lineMeterHolderRight, NULL);
								lv_obj_set_size(lineMeter5, 80, 80);
								lineMeterLabel5 = lv_label_create(lineMeter5, NULL);
								lv_label_set_text(lineMeterLabel5, "LML5");
								lv_obj_align(lineMeterLabel5, lineMeter5, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
								lv_lmeter_set_range(lineMeter5, 0, 360);
								//
								lineMeter6 = lv_lmeter_create(lineMeterHolderRight, NULL);
								lv_obj_set_size(lineMeter6, 80, 80);
								lineMeterLabel6 = lv_label_create(lineMeter6, NULL);
								lv_label_set_text(lineMeterLabel6, "LML6");
								lv_obj_align(lineMeterLabel6, lineMeter6, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
								lv_lmeter_set_range(lineMeter6, 0, 11);
								//
								lv_obj_align(lineMeter4, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);
								lv_obj_align(lineMeter5, NULL, LV_ALIGN_CENTER, 0, 0);
								lv_obj_align(lineMeter6, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);
								//

								//align the holders to the side
								lv_obj_align(lineMeterHolderLeft, scr, LV_ALIGN_IN_LEFT_MID, 0, 0);
								lv_obj_align(lineMeterHolderRight, scr, LV_ALIGN_IN_RIGHT_MID, 0, 0);
								//

								/*
								   lv_label_set_text(velLeft, "a");
								   lv_label_set_text(velRight, "b");
								   lv_obj_align(velLeft, NULL, LV_ALIGN_IN_TOP_LEFT, 5, 0);
								   lv_obj_align(velRight, NULL, LV_ALIGN_IN_TOP_RIGHT, -100, 0);
								   lv_label_set_text(posLeft, "a");
								   lv_label_set_text(posRight, "b");
								   lv_obj_align(posLeft, NULL, LV_ALIGN_IN_TOP_LEFT, 5, 20);
								   lv_obj_align(posRight, NULL, LV_ALIGN_IN_TOP_RIGHT, -100, 20);
								   lv_label_set_text(torqueLeft, "a");
								   lv_label_set_text(torqueRight, "b");
								   lv_obj_align(torqueLeft, NULL, LV_ALIGN_IN_TOP_LEFT, 5, 40);
								   lv_obj_align(torqueRight, NULL, LV_ALIGN_IN_TOP_RIGHT, -100, 40);
								 */
								lv_obj_align(batteryHolder, scr, LV_ALIGN_IN_BOTTOM_MID, -0, -65);
								lv_obj_set_size(batteryHolder, 170, 100);
								lv_obj_align(batteryCapacityLabel, batteryHolder, LV_ALIGN_IN_TOP_LEFT, 5, 0);
								lv_obj_align(batteryCurrentLabel, batteryHolder, LV_ALIGN_IN_TOP_RIGHT, -35, 0);
								lv_obj_align(batteryTempLabel, batteryHolder, LV_ALIGN_IN_BOTTOM_LEFT, 5, 0);
								lv_obj_align(batteryVoltageLabel, batteryHolder, LV_ALIGN_IN_BOTTOM_RIGHT, -40, 0);

								sprintf(ultLabel, "%lu cm", ultrasonic.get_value());
								lv_label_set_text(ultrasoniclabel, ultLabel);
								lv_obj_align(ultrasoniclabel, NULL, LV_ALIGN_IN_TOP_MID, 0, 5);

								//themes
								lv_theme_t * th = lv_theme_alien_init(1, NULL);
								lv_theme_set_current(th);
								lv_scr_load(scr);
}
int inchesToTicks(int inches){
	return 360*inches/(2*PI*2);
}
void uptake(double msec){
  intake.moveVelocity(200);
  delay(msec);
  intake.moveVelocity(0);
}
void downtake(double msec){
  intake.moveVelocity(-200);
  delay(msec);
  intake.moveVelocity(0);
}
void opcontrol() {
								unsigned int opStart = pros::millis();
								screenInit();
								right_dt_group.setReversed(false);
								flipper.setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
								drivetrain.setBrakeMode(okapi::AbstractMotor::brakeMode::coast);
								fly_group.setBrakeMode(okapi::AbstractMotor::brakeMode::coast);
								bool linedUp = false;
								double lightRange = .3;
								int defaultVelocity = 560;
								bool highFlag = false;
								bool middleFlag = false;
								bool lowFlag = false;

								bool red1High = false;
								bool red1Mid = false;
								bool red1Low = false;

								bool red2High = false;
								bool red2Mid = false;
								bool red2Low = false;

								bool red3High = false;
								bool red3Mid = false;
								bool red3Low = false;

								bool red1NotFlag = false;
								bool red2NotFlag = false;
								bool red3NotFlag = false;

								bool noFlag = false;
								while (true) {
																//setting up buttons
																vision_object_s_t red = vision_sensor.get_by_sig(0, SIG_RED);
																vision_object_s_t blue = vision_sensor.get_by_sig(0, SIG_BLUE);
																okapi::Controller controller;
																okapi::ControllerButton flySpinUp(ControllerDigital::X);
																okapi::ControllerButton flySpinDown(ControllerDigital::B);
																okapi::ControllerButton intakeUp(ControllerDigital::L1);
																okapi::ControllerButton intakeDown(ControllerDigital::L2);
																okapi::ControllerButton flipperUp(ControllerDigital::R1);
																okapi::ControllerButton flipperDown(ControllerDigital::R2);
																okapi::ControllerButton dtToggleBtn(ControllerDigital::A);
																bool dtToggle = false;
																okapi::ControllerButton visionAlign(ControllerDigital::Y);
																drivetrain.tank(controller.getAnalog(ControllerAnalog::leftY),
																																-controller.getAnalog(ControllerAnalog::rightY));
																//
																okapi::ControllerButton flyBackwards(ControllerDigital::left);
																okapi::ControllerButton visionTrack(ControllerDigital::up);
																//
																if(flySpinUp.isPressed()) {
																								fly_group.moveVelocity(defaultVelocity);
																}else if(flySpinDown.isPressed()) {
																								fly_group.moveVelocity(0);
																}else if(flyBackwards.isPressed()){
																								fly_group.moveVelocity(-defaultVelocity/5);
																}
																//
																if(intakeUp.isPressed()) {
																								std::cout << std::to_string(pros::millis()-opStart) + ", " + std::to_string(fly_group.getActualVelocity()) + '\n';
																								if(ultrasonic.get_value()<7){
																									primed = true;
																									//___int_least32_t_definedcontroller.rumble("- -");
																								}else{
																									primed = false;
																									//controller.rumble(". .");
																								}
																								intake.moveVelocity(175);
																}else if(intakeDown.isPressed()) {
																								intake.moveVelocity(-175);
																}else{
																								intake.moveVelocity(0);
																}
																//
																if(flipperUp.isPressed()) {
																								flipper.moveVelocity(200);
																}else if(flipperDown.isPressed()) {
																								flipper.moveVelocity(-200);
																}else{
																								flipper.moveVelocity(0);
																}
																//
																if(dtToggleBtn.isPressed()) {
																								delay(200);
																								if(dtToggle) drivetrain.setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
																								else if (!dtToggle) drivetrain.setBrakeMode(okapi::AbstractMotor::brakeMode::coast);
																								dtToggle = !dtToggle;
																}
																//
																if(visionAlign.changedToPressed()){
																	linedUp = false;
																	while(!linedUp&&visionAlign.isPressed()){
																		vision_object_s_t red = vision_sensor.get_by_sig(0, SIG_RED);
																		vision_object_s_t blue = vision_sensor.get_by_sig(0, SIG_BLUE);
																		if(isRed){
																			if(vision_sensor.get_object_count() > 0){
																				if(red.x_middle_coord>(316/2)+10){
																					drivetrain.left(.2);
																					drivetrain.right(.2);
																					controller.rumble("- ");
																				}else if(red.x_middle_coord<(316/2)-10){
																					drivetrain.right(-.2);
																					drivetrain.left(-.2);
																					controller.rumble(". ");
																				}else{
																					linedUp = true;
																					controller.rumble("--- ---");
																					drivetrain.stop();
																				}
																			}
																		}else{
																			if(blue.x_middle_coord>(VISION_FOV_WIDTH/2)+5){
																				drivetrain.left(.1);
																				drivetrain.right(-.05);
																			}else if(blue.x_middle_coord<(VISION_FOV_WIDTH/2)-5){
																				drivetrain.right(.1);
																				drivetrain.left(-.05);
																			}else{
																				linedUp = true;
																				drivetrain.stop();
																			}
																		}
																	}
																}
																//
																if(visionTrack.isPressed()){
																	while(visionTrack.isPressed()){



																		if(isRed){
																			vision_object_s_t red1 = vision_sensor.get_by_sig(0, SIG_RED);
																			vision_object_s_t red2 = vision_sensor.get_by_sig(1, SIG_RED);
																			vision_object_s_t red3 = vision_sensor.get_by_sig(2, SIG_RED);

																			double red1H = red1.height;
																			double red2H = red2.height;
																			double red3H = red3.height;

																			double red1Y = red1.y_middle_coord;
																			double red2Y = red2.y_middle_coord;
																			double red3Y = red3.y_middle_coord;

																			int red1HighYPredicted = -5.8944*red1H + 152.26;
																			int red2HighYPredicted = -5.8944*red2H + 152.26;
																			int red3HighYPredicted = -5.8944*red3H + 152.26;

																			int red1MidYPredicted = -2.8186*red1H + 146.71;
																			int red2MidYPredicted = -2.8186*red2H + 146.71;
																			int red3MidYPredicted = -2.8186*red3H + 146.71;

																			int red1LowYPredicted = -0.3421*red1H + 152.34;
																			int red2LowYPredicted = -0.3421*red2H + 152.34;
																			int red3LowYPredicted = -0.3421*red3H + 152.34;

																			int red1HighYError = abs(red1HighYPredicted - red1Y);
																			int red2HighYError = abs(red2HighYPredicted - red2Y);
																			int red3HighYError = abs(red3HighYPredicted - red3Y);

																			int red1MidYError = abs(red1MidYPredicted - red1Y);
																			int red2MidYError = abs(red2MidYPredicted - red2Y);
																			int red3MidYError = abs(red3MidYPredicted - red3Y);

																			int red1LowYError = abs(red1LowYPredicted - red1Y);
																			int red2LowYError = abs(red2LowYPredicted - red2Y);
																			int red3LowYError = abs(red3LowYPredicted - red3Y);

																			int red1HighDistance = 1462.5*pow(red1H,-1.028);
																			int red2HighDistance = 1462.5*pow(red2H,-1.028);
																			int red3HighDistance = 1462.5*pow(red3H,-1.028);

																			int red1MidDistance = 1005.6*pow(red1H,-0.869);
																			int red2MidDistance = 1005.6*pow(red2H,-0.869);
																			int red3MidDistance = 1005.6*pow(red2H,-0.869);

																			int red1LowDistance = 1248*pow(red1H,-0.936);
																			int red2LowDistance = 1248*pow(red2H,-0.936);
																			int red3LowDistance = 1248*pow(red3H,-0.936);

																			int highLaunchDistance = 34;
																			int midLaunchDistance = 14;


																			if (red1MidYError<20){
																				red1Mid = true;
																				middleFlag = true;
																			} else if (red1HighYError<20){
																				red1High = true;
																				middleFlag = true;
																			} else if (red1LowYError<20){
																				red1Low = true;
																				lowFlag = true;
																			} else {
																				red1NotFlag = true;
																			}
																			//
																			if (red2MidYError<20){
																				red2Mid = true;
																				middleFlag = true;
																			} else if (red2HighYError<20){
																				red2High = true;
																				highFlag = true;
																			} else if (red2LowYError<20){
																				red2Low = true;
																				lowFlag = true;
																			} else {
																				red2NotFlag = true;
																			}
																			//
																			if (red3HighYError<20){
																				red3Mid = true;
																				middleFlag = true;
																			} else if (red3HighYError<20){
																				red3High = true;
																				highFlag = true;
																			} else if (red3LowYError<20){
																				red3Low = true;
																				lowFlag = true;
																			} else {
																				red3NotFlag = true;
																			}
																			//
																			if (red1NotFlag&&red2NotFlag&&red3NotFlag){
																				noFlag = true;
																			}
																			if (noFlag){
																				controller.rumble("------");
																				break;
																			}
																			intake.moveVelocity(-100);
																			delay(200);
																			intake.moveVelocity(0);
																			fly_group.moveVelocity(defaultVelocity);
																			if (middleFlag){
																				if (red1Mid){
																					int error = red1MidDistance-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(300);
																					error = error-midLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(400);

																				} else if (red2Mid){
																					int error = red2MidDistance-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(300);
																					error = error-midLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(400);
																				} else if (red3Mid){
																					int error = red3MidDistance-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(300);
																					error = error-midLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(400);
																				}
																			} else if (highFlag){
																				if(red1High){
																					int error = red1HighDistance-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(300);
																					error = error-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(400);
																				} else if (red2High){
																					int error = red2HighDistance-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(300);
																					error = error-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(400);
																				}else if (red3High){
																					int error = red3HighDistance-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(300);
																					error = error-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(400);
																				}

																			} else if(lowFlag){
																				if(red1Low){
																					int error = red1LowDistance-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(300);
																					error = error-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(400);
																				} else if (red2Low){
																					int error = red2LowDistance-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(300);
																					error = error-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(400);
																				} else if (red3Low){
																					int error = red3LowDistance-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(300);
																					error = error-highLaunchDistance;
																					drivetrain.moveDistance(inchesToTicks(error));
																					uptake(400);
																				}
																			}



																		} else{
																			vision_object_s_t blue1 = vision_sensor.get_by_sig(0, SIG_BLUE);
																			vision_object_s_t blue2 = vision_sensor.get_by_sig(1, SIG_BLUE);
																			vision_object_s_t blue3 = vision_sensor.get_by_sig(2, SIG_BLUE);
																			if(blue.x_middle_coord>(VISION_FOV_WIDTH/2)+5){

																			}else if(blue.x_middle_coord<(VISION_FOV_WIDTH/2)-5){

																			}else{

																			}
																		}
																	}
																}
																//
																sprintf(batteryCapacityString, "%1.1f%%", pros::battery::get_capacity());
																sprintf(batteryCurrentString, "%lu mA", pros::battery::get_current());
																sprintf(batteryTempString, "%1.1f °C", pros::battery::get_temperature());
																sprintf(batteryVoltageString, "%lu mV", pros::battery::get_voltage());
																lv_label_set_text(batteryCapacityLabel, batteryCapacityString);
																lv_label_set_text(batteryCurrentLabel, batteryCurrentString);
																lv_label_set_text(batteryTempLabel, batteryTempString);
																lv_label_set_text(batteryVoltageLabel, batteryVoltageString);
																sprintf(ultLabel, "%lu cm", ultrasonic.get_value());
																lv_label_set_text(ultrasoniclabel, ultLabel);
																switch(screenPage) {
																case 0:
																								lv_label_set_text(toggleButtonLabel, pageTitles[screenPage]);
																								//
																								sprintf(velLeftString, "%1.3f rpm", okapi::Motor(LEFT_BACK_DT_PORT).getActualVelocity());
																								lv_lmeter_set_value(lineMeter1, fabs(okapi::Motor(LEFT_BACK_DT_PORT).getActualVelocity()));
																								lv_label_set_text(lineMeterLabel1, velLeftString);
																								sprintf(velRightString, "%1.3f rpm", -okapi::Motor(RIGHT_BACK_DT_PORT).getActualVelocity());
																								lv_lmeter_set_value(lineMeter4, fabs(okapi::Motor(RIGHT_BACK_DT_PORT).getActualVelocity()));
																								lv_label_set_text(lineMeterLabel4, velRightString);
																								//
																								sprintf(posLeftString, "%1.3f deg", okapi::Motor(LEFT_BACK_DT_PORT).getPosition());
																								lv_lmeter_set_value(lineMeter2, abs((int)(okapi::Motor(LEFT_BACK_DT_PORT).getPosition())%360));
																								lv_label_set_text(lineMeterLabel2, posLeftString);
																								sprintf(posRightString, "%1.3f deg", okapi::Motor(RIGHT_BACK_DT_PORT).getPosition());
																								lv_lmeter_set_value(lineMeter5, abs((int)(okapi::Motor(RIGHT_BACK_DT_PORT).getPosition())%360));
																								lv_label_set_text(lineMeterLabel5, posRightString);
																								//
																								sprintf(torqueLeftString, "%1.3f Nm", okapi::Motor(LEFT_BACK_DT_PORT).getTorque());
																								lv_lmeter_set_value(lineMeter3, okapi::Motor(LEFT_BACK_DT_PORT).getTorque());
																								lv_label_set_text(lineMeterLabel3, torqueLeftString);
																								sprintf(torqueRightString, "%1.3f Nm", okapi::Motor(RIGHT_BACK_DT_PORT).getTorque());
																								lv_lmeter_set_value(lineMeter6, okapi::Motor(RIGHT_BACK_DT_PORT).getTorque());
																								lv_label_set_text(lineMeterLabel6, torqueLeftString);
																								//
																								break;
																case 1:
																								lv_label_set_text(toggleButtonLabel, pageTitles[screenPage]);
																								//
																								sprintf(velLeftString, "%1.3f rpm", okapi::Motor(LEFT_FRONT_DT_PORT).getActualVelocity());
																								lv_lmeter_set_value(lineMeter1, fabs(okapi::Motor(LEFT_FRONT_DT_PORT).getActualVelocity()));
																								lv_label_set_text(lineMeterLabel1, velLeftString);
																								sprintf(velRightString, "%1.3f rpm", -okapi::Motor(RIGHT_FRONT_DT_PORT).getActualVelocity());
																								lv_lmeter_set_value(lineMeter4, fabs(okapi::Motor(RIGHT_FRONT_DT_PORT).getActualVelocity()));
																								lv_label_set_text(lineMeterLabel4, velRightString);
																								//
																								sprintf(posLeftString, "%1.3f deg", okapi::Motor(LEFT_FRONT_DT_PORT).getPosition());
																								lv_lmeter_set_value(lineMeter2, abs((int)(okapi::Motor(LEFT_FRONT_DT_PORT).getPosition())%360));
																								lv_label_set_text(lineMeterLabel2, posLeftString);
																								sprintf(posRightString, "%1.3f deg", okapi::Motor(RIGHT_FRONT_DT_PORT).getPosition());
																								lv_lmeter_set_value(lineMeter5, abs((int)(okapi::Motor(RIGHT_FRONT_DT_PORT).getPosition())%360));
																								lv_label_set_text(lineMeterLabel5, posRightString);
																								//
																								sprintf(torqueLeftString, "%1.3f Nm", okapi::Motor(LEFT_FRONT_DT_PORT).getTorque());
																								lv_lmeter_set_value(lineMeter3, okapi::Motor(LEFT_FRONT_DT_PORT).getTorque());
																								lv_label_set_text(lineMeterLabel3, torqueLeftString);
																								sprintf(torqueRightString, "%1.3f Nm", okapi::Motor(RIGHT_FRONT_DT_PORT).getTorque());
																								lv_lmeter_set_value(lineMeter6, okapi::Motor(RIGHT_FRONT_DT_PORT).getTorque());
																								lv_label_set_text(lineMeterLabel6, torqueLeftString);
																								//
																								break;
																case 2:
																								lv_label_set_text(toggleButtonLabel, pageTitles[screenPage]);
																								//
																								sprintf(velLeftString, "%1.3f rpm", okapi::Motor(LEFT_FLY_PORT).getActualVelocity());
																								lv_lmeter_set_value(lineMeter1, fabs(okapi::Motor(LEFT_FLY_PORT).getActualVelocity()));
																								lv_label_set_text(lineMeterLabel1, velLeftString);
																								sprintf(velRightString, "%1.3f rpm", -okapi::Motor(RIGHT_FLY_PORT).getActualVelocity());
																								lv_lmeter_set_value(lineMeter4, fabs(okapi::Motor(RIGHT_FLY_PORT).getActualVelocity()));
																								lv_label_set_text(lineMeterLabel4, velRightString);
																								//
																								sprintf(posLeftString, "%1.3f deg", okapi::Motor(LEFT_FLY_PORT).getPosition());
																								lv_lmeter_set_value(lineMeter2, abs((int)(okapi::Motor(LEFT_FLY_PORT).getPosition())%360));
																								lv_label_set_text(lineMeterLabel2, posLeftString);
																								sprintf(posRightString, "%1.3f deg", okapi::Motor(RIGHT_FLY_PORT).getPosition());
																								lv_lmeter_set_value(lineMeter5, abs((int)(okapi::Motor(RIGHT_FLY_PORT).getPosition())%360));
																								lv_label_set_text(lineMeterLabel5, posRightString);
																								//
																								sprintf(torqueLeftString, "%1.3f Nm", okapi::Motor(LEFT_FLY_PORT).getTorque());
																								lv_lmeter_set_value(lineMeter3, okapi::Motor(LEFT_FLY_PORT).getTorque());
																								lv_label_set_text(lineMeterLabel3, torqueLeftString);
																								sprintf(torqueRightString, "%1.3f Nm", okapi::Motor(RIGHT_FLY_PORT).getTorque());
																								lv_lmeter_set_value(lineMeter6, okapi::Motor(RIGHT_FLY_PORT).getTorque());
																								lv_label_set_text(lineMeterLabel6, torqueLeftString);
																								//
																								break;
																case 3:
																								lv_label_set_text(toggleButtonLabel, pageTitles[screenPage]);
																								sprintf(velLeftString, "%1.3f rpm", intake.getActualVelocity());
																								lv_lmeter_set_value(lineMeter1, fabs(intake.getActualVelocity()));
																								lv_label_set_text(lineMeterLabel1, velLeftString);
																								sprintf(velRightString, "%1.3f rpm", -flipper.getActualVelocity());
																								lv_lmeter_set_value(lineMeter4, fabs(flipper.getActualVelocity()));
																								lv_label_set_text(lineMeterLabel4, velRightString);
																								//
																								sprintf(posLeftString, "%1.3f deg", intake.getPosition());
																								lv_lmeter_set_value(lineMeter2, abs((int)(intake.getPosition())%360));
																								lv_label_set_text(lineMeterLabel2, posLeftString);
																								sprintf(posRightString, "%1.3f deg", flipper.getPosition());
																								lv_lmeter_set_value(lineMeter5, abs((int)(flipper.getPosition())%360));
																								lv_label_set_text(lineMeterLabel5, posRightString);
																								//
																								sprintf(torqueLeftString, "%1.3f Nm", intake.getTorque());
																								lv_lmeter_set_value(lineMeter3, intake.getTorque());
																								lv_label_set_text(lineMeterLabel3, torqueLeftString);
																								sprintf(torqueRightString, "%1.3f Nm", flipper.getTorque());
																								lv_lmeter_set_value(lineMeter6, flipper.getTorque());
																								lv_label_set_text(lineMeterLabel6, torqueLeftString);
																								break;
																}
																pros::delay(10);
								}
}
