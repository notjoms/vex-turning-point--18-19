#ifndef _MOTORDEFINE_H_
#define _MOTORDEFINE_H_
int motorLookup(char* motorString);
int analogSensorLookup(char* sensorString);
int digitalSensorLookup(char* sensorString);
char* reverseMotorLookup(int a);
char* reverseAnalogSensorLookup(int a);
char* reverseDigitalSensorLookup(int a);
#endif
