#ifndef _FLYWHEEL_H_
#define _FLYWHEEL_H_
int slewSteps[3];
float angVelocity;
double kP, kI, kD;
float currentRPM, reqRPM;
float error;
float integral, derivative, lastError;
void setFlyPower(int power);
void setVelocity(int velocity, float predicted_drive);
float calcAngVelocity();
int updateVelocity();
int speedManagementTask();
void setFlySpeed(float rpm);
void flywheel();
void flyWheelSlewUp(int seconds);
void flyWheelSlewDown(int seconds);
#endif
