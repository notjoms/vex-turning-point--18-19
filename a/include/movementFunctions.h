#ifndef _MOVEMENTFUNCTIONS_H_
#define _MOVEMENTFUNCTIONS_H_
float inchesToEncoderCounts(float inches);
void moveForwards(float inches);
void move(float time, float power);
void moveBackwards(float inches);
float gyroScale(float gyroScale);
void turn(float degrees);
void autoIntakeUp(float time);
void autoIntakeDown(float time);
void autoIntakeDriving(float time, int inches, int intakePower, int drivePower);
void flyWheelSlewUpAuton(float seconds);
void flyWheelSlewDownAuton(float seconds);
#endif
