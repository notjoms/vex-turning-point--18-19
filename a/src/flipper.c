#include "main.h"
#include "flipper.h"
void flipper(){
  if(joystickGetDigital(1,6,JOY_UP))
    motorSet(motorLookup("flipper"),63);

  else if(joystickGetDigital(1,6,JOY_DOWN)
    && digitalRead(digitalSensorLookup("flipperLimitSwitch"))==HIGH)
    motorSet(motorLookup("flipper"),-63);

  else motorSet(motorLookup("flipper"), 0);
  
}
