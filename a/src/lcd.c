#include "main.h"
#include "lcd.h"
void lcd(float a){
  lcdClear(uart1);
  lcdPrint(uart1, 1, "%4.1f rpm", a);
  lcdPrint(uart1, 2, "%d deg", gyroGet(gyro));
}
