/** @file opcontrol.c
 * @brief File for operator control code
 *
 * This file should contain the user operatorControl() function and any functions related to it.
 *
 * Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/
 *

 * PROS contains FreeRTOS (http://www.freertos.org) whose source code may be
 * obtained from http://sourceforge.net/projects/freertos/files/ or on request.
 */

#include "main.h"
#include "drivetrain.h"
#include "flipper.h"
#include "flywheel.h"
#include "lcd.h"
#include "intake.h"
#include "JINXDebugging.h"
#include "lcdSelector.h"
void operatorControl() {
	float flyEncCount = 0, flyEncCountLast = 0;
	float angVelocity;
	while (1) {
		drivetrain();
		flipper();
		intake();
		flywheel();
		delay(20);
	}
}
