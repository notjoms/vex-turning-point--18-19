#include "main.h"
#include "JINXDebugging.h"
#include "motorDefine.h"
void debugCommands(){
  char buffer[50];
  for(int i = 0; i < 10; i++){
    sprintf(buffer, "%d", motorGet(i+1));
    writeJINXData(reverseMotorLookup(i), buffer);
  }
  for(int i = 0; i < 8; i++){
    sprintf(buffer, "%d", analogReadCalibrated(i+1));
    writeJINXData(reverseAnalogSensorLookup(i), buffer);
  }
  for(int i = 0; i < 12; i++){
    sprintf(buffer, "%d", digitalRead(i=1));
    writeJINXData(reverseDigitalSensorLookup(i), buffer);
  }
}
