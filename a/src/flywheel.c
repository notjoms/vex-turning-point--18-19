#include "main.h"
#include "drivetrain.h"
#include "flipper.h"
#include "flywheel.h"
#include "lcd.h"
#include "intake.h"
#include "truespeed.h"
const float RPM_GOAL = 2850.0f;
int spinUpTime = 4, spinDownTime = 5;
int slewSteps[3] = {40,50,80};
int flyEncCount = 0, flyEncCountLast = 0;
extern float angVelocity;
float wSmooth = 0;
float currentRPM = 0, reqRPM = 0;
float error = 0;
double kP = .45, kI = 0/*1.267605634*/, kD = 0.0399375;
void setFlyPower(int power){
  motorSet(motorLookup("lFlywheel"), power);
  motorSet(motorLookup("rFlywheel"), -power);
}
float calcAngVelocity(){
  flyEncCount = encoderGet(flyEncoder);
  angVelocity = 0;
  float ticks_per_millisecond = (flyEncCount - flyEncCountLast)/(20);
  angVelocity = ticks_per_millisecond / 360 * 60000;
  wSmooth = (2*wSmooth+angVelocity)/3;
  flyEncCountLast = flyEncCount;
  return fabs(wSmooth*5);
}
int speedManagementTask(){
  currentRPM = calcAngVelocity();
  lcd(currentRPM);
  int power = 0;
  int prevPower = motorGet(motorLookup("lFlywheel"));
  error = reqRPM - currentRPM;
  integral += error;
  derivative = error - lastError;
  power = (kP*error)+(kI*integral)+(kD*derivative);
  if(currentRPM>RPM_GOAL + 50.0f) power = prevPower - 2;
  if(power>127)power=127;
  else if(power<0)power=0;
  lastError = error;
  return power;
}
void flywheel(){
  int power = speedManagementTask();
  if(joystickGetDigital(1,8,JOY_UP)){
    reqRPM = RPM_GOAL;
    setFlyPower(power);
	}
	else if(joystickGetDigital(1,8,JOY_DOWN)){
    reqRPM = 0;
    setFlyPower(0);
	}
	else if(joystickGetDigital(1,8,JOY_LEFT)){
    reqRPM = 0;
    motorStop(motorLookup("lFlywheel"));
    motorStop(motorLookup("rFlywheel"));
	}
	else if(joystickGetDigital(1,8,JOY_RIGHT)){
		setFlyPower(motorGet(motorLookup("lFlywheel"))+1);
	}
  else if(joystickGetDigital(1, 7, JOY_UP)){
    reqRPM = RPM_GOAL*1.25;
    setFlyPower(power);
  }
  else if(joystickGetDigital(1, 7, JOY_DOWN)){
    reqRPM = RPM_GOAL*.75;
    setFlyPower(80);
  }
  else if(joystickGetDigital(1, 7, JOY_LEFT)){
    reqRPM = 0;
    setFlyPower(0);
    motorSet(motorLookup("lFlywheel"), 80);
    motorSet(motorLookup("rFlywheel"), -80);
  }
  else{
    setFlyPower(power);
  }

}
void flyWheelSlewUp(int seconds){
  /*if(motorGet(motorLookup("lFlywheel"))<90){
    int flyEncCount = 0, flyEncCountLast = 0;
  	float angVelocity;
    motorSet(motorLookup("lFlywheel"), 0);
    motorSet(motorLookup("rFlywheel"), 0);
    float lastFiveCount[5] = {0,0,0,0,0};
  	for(int x = 0; x<=seconds*50; x++){
  	   motorSet(motorLookup("lFlywheel"), motorGet(motorLookup("lFlywheel"))+returnTrueSpeed(90/(seconds*50)));
  	   motorSet(motorLookup("rFlywheel"), motorGet(motorLookup("rFlywheel"))-returnTrueSpeed(90/(seconds*50)));
       drivetrain();
       flipper();
       flyEncCount = encoderGet(flyEncoder);
       float ticks_per_millisecond = (flyEncCount - flyEncCountLast)/(20);
		   angVelocity = ticks_per_millisecond / 360 * 60000;
		   flyEncCountLast = flyEncCount;
   		 lcd(angVelocity*5);
       intake();
       if(joystickGetDigital(1,8,JOY_DOWN)){
     		  flyWheelSlewDown(spinDownTime);
          break;
     	 }
       delay(20);
    }
  }
  motorSet(motorLookup("lFlywheel"), 90);
  motorSet(motorLookup("rFlywheel"), -90);*/
  motorSet(motorLookup("lFlywheel"), slewSteps[0]);
  motorSet(motorLookup("rFlywheel"), -slewSteps[0]);
  unsigned long loopStartTime = millis();
  int flyEncCount = 0, flyEncCountLast = 0;
  float angVelocity;
  while(seconds*1000/2 < (millis() - loopStartTime)){
    flyEncCount = encoderGet(flyEncoder);
    float ticks_per_millisecond = (flyEncCount - flyEncCountLast)/(20);
    angVelocity = ticks_per_millisecond / 360 * 60000;
    flyEncCountLast = flyEncCount;
    lcd(angVelocity*5);
  }
  motorSet(motorLookup("lFlywheel"), slewSteps[1]);
  motorSet(motorLookup("rFlywheel"), -slewSteps[1]);
  while(seconds*1000/2 < (millis() - loopStartTime)){
    flyEncCount = encoderGet(flyEncoder);
    float ticks_per_millisecond = (flyEncCount - flyEncCountLast)/(20);
    angVelocity = ticks_per_millisecond / 360 * 60000;
    flyEncCountLast = flyEncCount;
    lcd(angVelocity*5);
  }
  motorSet(motorLookup("lFlywheel"), slewSteps[2]);
  motorSet(motorLookup("rFlywheel"), -slewSteps[2]);

}
void flyWheelSlewDown(int seconds){
  /*if(motorGet(motorLookup("lFlywheel"))>0){
    int flyEncCount = 0, flyEncCountLast = 0;
  	float angVelocity;
    motorSet(motorLookup("lFlywheel"), 90);
    motorSet(motorLookup("rFlywheel"), -90);
  	for(int x = 0; x<=seconds*50; x++){
  	   motorSet(motorLookup("lFlywheel"), motorGet(motorLookup("lFlywheel"))-returnTrueSpeed(90/(seconds*50)));
  	   motorSet(motorLookup("rFlywheel"), motorGet(motorLookup("rFlywheel"))+returnTrueSpeed(90/(seconds*50)));
       drivetrain();
       flipper();
       flyEncCount = encoderGet(flyEncoder);
		   float ticks_per_millisecond = (flyEncCount - flyEncCountLast)/(20);
		   angVelocity = ticks_per_millisecond / 360 * 60000;
		   flyEncCountLast = flyEncCount;
   		 lcd(angVelocity*5);
       intake();
       if(joystickGetDigital(1,8,JOY_UP)){
     			flyWheelSlewUp(spinUpTime);
          break;
     	 }
       delay(20);
    }
  }
  motorSet(motorLookup("lFlywheel"), 0);
  motorSet(motorLookup("rFlywheel"), 0);
  */
  motorSet(motorLookup("lFlywheel"), slewSteps[1]);
  motorSet(motorLookup("rFlywheel"), -slewSteps[1]);
  unsigned long loopStartTime = millis();
  int flyEncCount = 0, flyEncCountLast = 0;
  float angVelocity;
  while(seconds*1000/2 < (millis() - loopStartTime)){
    flyEncCount = encoderGet(flyEncoder);
    float ticks_per_millisecond = (flyEncCount - flyEncCountLast)/(20);
    angVelocity = ticks_per_millisecond / 360 * 60000;
    flyEncCountLast = flyEncCount;
    lcd(angVelocity*5);
  }
  motorSet(motorLookup("lFlywheel"), slewSteps[0]);
  motorSet(motorLookup("rFlywheel"), -slewSteps[0]);
  while(seconds*1000/2 < (millis() - loopStartTime)){
    flyEncCount = encoderGet(flyEncoder);
    float ticks_per_millisecond = (flyEncCount - flyEncCountLast)/(20);
    angVelocity = ticks_per_millisecond / 360 * 60000;
    flyEncCountLast = flyEncCount;
    lcd(angVelocity*5);
  }
  motorSet(motorLookup("lFlywheel"), 0);
  motorSet(motorLookup("rFlywheel"), 0);


}
