#include "main.h"
#include "lcdSelector.h"
#define kButtonNone 0
#define kButtonLeft LCD_BTN_LEFT
#define kButtonCenter LCD_BTN_CENTER
#define kButtonRight LCD_BTN_RIGHT
int lcdCount = 0;
void waitForPress(){
  while(lcdReadButtons(uart1) == 0) delay(5);
}
void waitForRelease(){
  while(lcdReadButtons(uart1) != 0) delay(5);
}
int getLcdButtons(){
    int buttons;
    // This function will block until either
    // 1. A button is pressed on the LCD
    //    If a button is pressed when the function starts then that button
    //    must be released before a new button is detected.
    // 2. Robot competition state changes

    // Wait for all buttons to be released
    while( lcdReadButtons(uart1) != kButtonNone ) {
        // check competition state, bail if it changes
        if( isEnabled() )
            return( kButtonNone );
        taskDelay(10);
    }

    // block until an LCD button is pressed
    do  {
        // we use a copy of the lcd buttons to avoid their state changing
        // between the test and returning the status
        buttons = lcdReadButtons(uart1);

        // check competition state, bail if it changes
        if( isEnabled() )
            return( kButtonNone );

        taskDelay(10);
        } while( buttons == kButtonNone );

    return( buttons );
}
void LcdSetAutonomous( int value ){
    // Simple selection display
    if( value == 0 ) {
      lcdSetText(uart1, 1, "auton 0/blue front");
      lcdSetText(uart1, 2, "[00] 01  02  03");
    }
    if( value == 1 ) {
      lcdSetText(uart1, 1, "auton 1/red front");
      lcdSetText(uart1, 2, " 00 [01] 02  03 ");
    }
    if( value == 2 ) {
      lcdSetText(uart1, 1, "auton 2");
      lcdSetText(uart1, 2, " 00  01 [02] 03 ");
    }
    if( value == 3 ) {
      lcdSetText(uart1, 1, "auton 3");
      lcdSetText(uart1, 2, " 00  01  02 [03]");
    }

    // Save autonomous mode for later
    lcdCount = value;
}
void LcdAutonomousSelection(){
    int button;

    // Clear LCD and turn on backlight
    lcdClear(uart1);
    lcdSetBacklight(uart1,true);

    // diaplay default choice
    LcdSetAutonomous(0);
    // PROS seems to need a delay
    taskDelay(2000);

    while(!isEnabled()){
        // this function blocks until button is pressed
        button = getLcdButtons();

        // Display and select the autonomous routine
        if( button == kButtonLeft )
            LcdSetAutonomous(0);

        if( button == kButtonCenter )
            LcdSetAutonomous(1);

        if( button == kButtonRight ){
          LcdSetAutonomous(2);
          wait(500);
          button = getLcdButtons();
          if( button == kButtonRight ){
            LcdSetAutonomous(3);
          }
        }

        // Don't hog the cpu !
        taskDelay(10);
      }

    lcdSetText(uart1, 2, "Running....     ");
}
