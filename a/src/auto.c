/** @file auto.c
 * @brief File for autonomous code
 *
 * This file should contain the user autonomous() function and any functions related to it.
 *
 * Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/
 *
 * PROS contains FreeRTOS (http://www.freertos.org) whose source code may be
 * obtained from http://sourceforge.net/projects/freertos/files/ or on request.
 */

#include "main.h"
#include "movementFunctions.h"
#include "lcdSelector.h"
#include "flywheel.h"
extern int lcdCount;
extern float RPM_GOAL;
void autonomous() {
  unsigned long startTime;
  switch (lcdCount) {
    case 0: //Front Right Auton (BLUE)
      lcdClear(uart1);
      lcdSetText(uart1, 1, "Auton 0");
      reqRPM = 0;
      gyroReset(gyro);
      wait(100);
      moveForwards(56);
      autoIntakeUp(1.6);
      autoIntakeDown(.15);
      setFlyPower(90);
      moveBackwards(60);
      turn(-86);
      moveBackwards(10);
      autoIntakeUp(.12);
      moveForwards(35);
      autoIntakeDown(.1);
      autoIntakeUp(.7);
      moveForwards(24);
      reqRPM = 0;
      motorSet(motorLookup("intake"),-30);
      setFlyPower(0);
      wait(300);
      moveBackwards(12);
      motorSet(motorLookup("intake"),0);
      turn(100);
      motorSet(motorLookup("intake"), -100);
      moveForwards(20);
      wait(200);
      moveForwards(13);
      moveBackwards(9);
      motorStopAll();
      break;
    case 1: //Front Left Auton (RED)
      lcdClear(uart1);
      lcdSetText(uart1, 1, "Auton 1");
      reqRPM = 0;
      gyroReset(gyro);
      wait(100);
      moveForwards(56);
      autoIntakeUp(1.6);
      autoIntakeDown(.15);
      setFlyPower(90);
      moveBackwards(60);
      turn(86);
      moveBackwards(10);
      autoIntakeUp(.15);
      moveForwards(35);
      autoIntakeDown(.1);
      autoIntakeUp(.7);
      moveForwards(32);
      reqRPM = 0;
      motorSet(motorLookup("intake"),-30);
      setFlyPower(0);
      wait(300);
      moveBackwards(12);
      motorSet(motorLookup("intake"),0);
      turn(-120);
      motorSet(motorLookup("intake"), -100);
      moveForwards(20);
      wait(200);
      moveForwards(13);
      moveBackwards(9);
      motorStopAll();
      break;
    case 2:
      lcdClear(uart1);
      lcdSetText(uart1, 1, "Auton 2");
      gyroReset(gyro);
      wait(100);
      moveForwards(57);
      autoIntakeUp(1.7);
      autoIntakeDown(.3);
      reqRPM = RPM_GOAL;
      setFlyPower(70);
      moveBackwards(12);
      turn(-90);
      motorSet(motorLookup("FLDT"), 100);
      motorSet(motorLookup("BLDT"), 100);
      motorSet(motorLookup("FRDT"), -100);
      motorSet(motorLookup("BRDT"), -100);
      wait(1400);
      motorSet(motorLookup("FLDT"), -20);
      motorSet(motorLookup("BLDT"), -20);
      motorSet(motorLookup("FRDT"), 20);
      motorSet(motorLookup("BRDT"), 20);
      wait(200);
      motorSet(motorLookup("FLDT"), 0);
      motorSet(motorLookup("BLDT"), 0);
      motorSet(motorLookup("FRDT"), 0);
      motorSet(motorLookup("BRDT"), 0);
      wait(300);
      turn(5);
      wait(1000);
      autoIntakeUp(.3);
      setFlyPower(0);
      wait(200);
      autoIntakeUp(.3);
      motorStopAll();
      break;
    case 3:
      lcdClear(uart1);
      lcdSetText(uart1, 1, "Auton 3");
      gyroReset(gyro);
      wait(100);
      moveForwards(57);
      autoIntakeUp(1.7);
      moveBackwards(10);
      autoIntakeDown(.3);
      reqRPM = RPM_GOAL;
      setFlyPower(75);
      turn(90);
      motorSet(motorLookup("FLDT"), 100);
      motorSet(motorLookup("BLDT"), 100);
      motorSet(motorLookup("FRDT"), -100);
      motorSet(motorLookup("BRDT"), -100);
      wait(1500);
      motorSet(motorLookup("FLDT"), -20);
      motorSet(motorLookup("BLDT"), -20);
      motorSet(motorLookup("FRDT"), 20);
      motorSet(motorLookup("BRDT"), 20);
      wait(200);
      motorSet(motorLookup("FLDT"), 0);
      motorSet(motorLookup("BLDT"), 0);
      motorSet(motorLookup("FRDT"), 0);
      motorSet(motorLookup("BRDT"), 0);
      turn(-5);
      wait(1000);
    //  autoIntakeUp(.3);
      motorStopAll();
      break;
    default:
      break;
  }
}
