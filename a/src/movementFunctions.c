#include "main.h"
#include "movementFunctions.h"
#include "truespeed.h"
#include "flywheel.h"
const float radiusOfWheels = 2;
const float radiusOfRobot = 7;
extern int lcdCount;
extern const float RPM_GOAL;
float dtkP = .4, dtkI = .1, dtkD = .01;
float gyrokP = .5, gyrokI = .1, gyrokD = .01;
float inchesToEncoderCounts(float inches){
  return inches*360*.5/(2*3.14159*radiusOfWheels);
}
void moveForwards(float inches){
  int encoderCountsNeeded = inchesToEncoderCounts(inches);
  encoderReset(leftEncoder);
  encoderReset(rightEncoder);
  int maxPower = .1*(inches*inches) + 3*inches;
  if (maxPower<50){
    int maxPower = 50;
  }
  motorSet(motorLookup("FLDT"), 25);
  motorSet(motorLookup("BLDT"), 25);
  motorSet(motorLookup("FRDT"), -25);
  motorSet(motorLookup("BRDT"), -25);
  wait(100);
  motorSet(motorLookup("FLDT"), 45);
  motorSet(motorLookup("BLDT"), 45);
  motorSet(motorLookup("FRDT"), -45);
  motorSet(motorLookup("BRDT"), -45);
  wait(50);
  while(encoderGet(leftEncoder)>-encoderCountsNeeded && encoderGet(rightEncoder)<encoderCountsNeeded){
    int power = 0;
    error = encoderGet(leftEncoder) - encoderCountsNeeded;
    power = dtkP * fabs(error);
    if(power < 33) power = 33;
    if(power > maxPower) power = maxPower;
    motorSet(motorLookup("FLDT"), power);
    motorSet(motorLookup("BLDT"), power);
    motorSet(motorLookup("FRDT"), -power);
    motorSet(motorLookup("BRDT"), -power);
  }
  encoderReset(leftEncoder);
  encoderReset(rightEncoder);
  /*motorSet(motorLookup("FLDT"), -maxPower/8);
  motorSet(motorLookup("BLDT"), -maxPower/8);
  motorSet(motorLookup("FRDT"), maxPower/8);
  motorSet(motorLookup("BRDT"), maxPower/8);
  wait(20);*/
  motorSet(motorLookup("FLDT"), 0);
  motorSet(motorLookup("BLDT"), 0);
  motorSet(motorLookup("FRDT"), 0);
  motorSet(motorLookup("BRDT"), 0);
  wait(100);
}
void move(float time, float power){
  motorSet(motorLookup("FLDT"), -power);
  motorSet(motorLookup("BLDT"), -power);
  motorSet(motorLookup("FRDT"), power);
  motorSet(motorLookup("BRDT"), power);
  wait(time*1000);
  motorSet(motorLookup("FLDT"), 0);
  motorSet(motorLookup("BLDT"), 0);
  motorSet(motorLookup("FRDT"), 0);
  motorSet(motorLookup("BRDT"), 0);
}
void moveBackwards(float inches){
  int encoderCountsNeeded = inchesToEncoderCounts(inches);
  encoderReset(leftEncoder);
  encoderReset(rightEncoder);
  int maxPower = .1*(inches*inches) + 3*inches;
  if (maxPower<30){
    int maxPower = 30;
  }
  if (maxPower>80){
    int maxPower = 80;
  }

  motorSet(motorLookup("FLDT"), -45);
  motorSet(motorLookup("BLDT"), -45);
  motorSet(motorLookup("FRDT"), 45);
  motorSet(motorLookup("BRDT"), 45);
  wait(100);
  while(encoderGet(leftEncoder)<encoderCountsNeeded && encoderGet(rightEncoder)>-encoderCountsNeeded){
    int power = 0;
    error = encoderCountsNeeded - encoderGet(leftEncoder);
    power = dtkP * fabs(error);
    if(power < 20) power = 20;
    if(power > maxPower) power = maxPower;
    motorSet(motorLookup("FLDT"), -power);
    motorSet(motorLookup("BLDT"), -power);
    motorSet(motorLookup("FRDT"), power);
    motorSet(motorLookup("BRDT"), power);
  }
  encoderReset(leftEncoder);
  encoderReset(rightEncoder);
  /*motorSet(motorLookup("FLDT"), maxPower/8);
  motorSet(motorLookup("BLDT"), maxPower/8);
  motorSet(motorLookup("FRDT"), -maxPower/8);
  motorSet(motorLookup("BRDT"), -maxPower/8);
  wait(20);*/
  motorSet(motorLookup("FLDT"), 0);
  motorSet(motorLookup("BLDT"), 0);
  motorSet(motorLookup("FRDT"), 0);
  motorSet(motorLookup("BRDT"), 0);
  wait(100);
}

float gyroScale(float gyroScale){
	return gyroGet(gyro)*gyroScale;
}

void turn(float degrees){
  int direction;
	float gyroScaleFloat = 1.07;
  int maxPower = 70;
  motorSet(motorLookup("FLDT"), 0);
  motorSet(motorLookup("BLDT"), 0);
  motorSet(motorLookup("FRDT"), 0);
  motorSet(motorLookup("BRDT"), 0);
  delay(200);
  gyroReset(gyro);
	if(degrees < 0) direction = 1;//negative/clockwise
	if(degrees > 0) direction = 0;//positive/counterclockwise
	if(direction == 1){//if turn is clockwise
		while(gyroScale(gyroScaleFloat)>degrees){//gyro negative
			//error = fabs(gyroScale(gyroScaleFloat)) - fabs(degrees);
      int power = 0;
      error = degrees - gyroGet(gyro);
      power = gyrokP * fabs(error);
      if(power < 24) power = 24;
      if(power > maxPower) power = maxPower;
      //motors positive
			motorSet(motorLookup("FLDT"), power);
      motorSet(motorLookup("BLDT"), power);
      motorSet(motorLookup("FRDT"), power);
      motorSet(motorLookup("BRDT"), power);
		}
    motorSet(motorLookup("FLDT"), 0);
    motorSet(motorLookup("BLDT"), 0);
    motorSet(motorLookup("FRDT"), 0);
    motorSet(motorLookup("BRDT"), 0);
	}else if(direction == 0){//if turn is counterclockwise
		while(gyroScale(gyroScaleFloat)<degrees){//gyro positive
			//error = -degrees + gyroScale(gyroScaleFloat);
      int power = 0;
      error = degrees - gyroGet(gyro);
      power = gyrokP * fabs(error);
      if(power < 24) power = 24;
      if(power > maxPower) power = maxPower;
      //motors negative
      motorSet(motorLookup("FLDT"), -power);
      motorSet(motorLookup("BLDT"), -power);
      motorSet(motorLookup("FRDT"), -power);
      motorSet(motorLookup("BRDT"), -power);
		}
    motorSet(motorLookup("FLDT"), 0);
    motorSet(motorLookup("BLDT"), 0);
    motorSet(motorLookup("FRDT"), 0);
    motorSet(motorLookup("BRDT"), 0);
	}
  motorSet(motorLookup("FLDT"), 0);
  motorSet(motorLookup("BLDT"), 0);
  motorSet(motorLookup("FRDT"), 0);
  motorSet(motorLookup("BRDT"), 0);
}

void autoIntakeUp(float time){
  motorSet(motorLookup("intake"),127);
  delay(time*1000);
  motorSet(motorLookup("intake"),0);
}

void autoIntakeDown(float time){
  motorSet(motorLookup("intake"),-127);
  delay(time*1000);
  motorSet(motorLookup("intake"),0);
}

void autoIntakeDriving(float time, int inches, int intakePower, int drivePower){
  int encoderCountsNeeded = inchesToEncoderCounts(inches);
  encoderReset(leftEncoder);
  encoderReset(rightEncoder);
  unsigned long getStartTime = millis();
  motorSet(motorLookup("intake"), 127);
  while(encoderGet(leftEncoder)<encoderCountsNeeded && encoderGet(rightEncoder)>-encoderCountsNeeded){
    int power = 0;
    error = encoderGet(leftEncoder) - encoderCountsNeeded;
    power = dtkP * fabs(error);
    if(power < 20) power = 20;
    if(power > drivePower) power = drivePower;
    motorSet(motorLookup("FLDT"), power);
    motorSet(motorLookup("BLDT"), power);
    motorSet(motorLookup("FRDT"), -power);
    motorSet(motorLookup("BRDT"), -power);
    if(millis()>getStartTime+time*1000){
      motorSet(motorLookup("intake"), 0);
    }
  }
  motorSet(motorLookup("intake"), 0);
  motorSet(motorLookup("FLDT"), 0);
  motorSet(motorLookup("BLDT"), 0);
  motorSet(motorLookup("FRDT"), 0);
  motorSet(motorLookup("BRDT"), 0);
  encoderReset(leftEncoder);
  encoderReset(rightEncoder);
}

void flyWheelSlewUpAuton(float seconds){
  motorSet(motorLookup("lFlywheel"), 0);
  motorSet(motorLookup("rFlywheel"), 0);
  wait(seconds*1000/3);
  motorSet(motorLookup("lFlywheel"), slewSteps[0]);
  motorSet(motorLookup("rFlywheel"), -slewSteps[0]);
  wait(seconds*1000/3);
  motorSet(motorLookup("lFlywheel"), slewSteps[1]);
  motorSet(motorLookup("rFlywheel"), -slewSteps[1]);
  wait(seconds*1000/3);
  motorSet(motorLookup("lFlywheel"), slewSteps[2]);
  motorSet(motorLookup("rFlywheel"), -slewSteps[2]);
}

void flyWheelSlewDownAuton(float seconds){
  motorSet(motorLookup("lFlywheel"), slewSteps[2]);
  motorSet(motorLookup("rFlywheel"), -slewSteps[2]);
  wait(seconds*1000/3);
  motorSet(motorLookup("lFlywheel"), slewSteps[1]);
  motorSet(motorLookup("rFlywheel"), -slewSteps[1]);
  wait(seconds*1000/3);
  motorSet(motorLookup("lFlywheel"), slewSteps[0]);
  motorSet(motorLookup("rFlywheel"), -slewSteps[0]);
  wait(seconds*1000/3);
  motorSet(motorLookup("lFlywheel"), 0);
  motorSet(motorLookup("rFlywheel"), 0);
}
