#include "main.h"
#include "intake.h"
void intake(){
  if(joystickGetDigital(1,5,JOY_UP)) motorSet(motorLookup("intake"),-127);
  else if(joystickGetDigital(1,5,JOY_DOWN)) motorSet(motorLookup("intake"),127);
  else motorSet(motorLookup("intake"), 0);
}
