#include "main.h"
#include "launcher.h"
void launcher(){
  if(joystickGetDigital(1,7,JOY_UP)) motorSet(motorLookup("launcher"),127);
  else if(joystickGetDigital(1,7,JOY_DOWN)) motorSet(motorLookup("launcher"),-127);
  else motorSet(motorLookup("launcher"), 0);
}
