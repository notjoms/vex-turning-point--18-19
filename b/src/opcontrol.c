/** @file opcontrol.c
 * @brief File for operator control code
 *
 * This file should contain the user operatorControl() function and any functions related to it.
 *
 * PROS contains FreeRTOS (http://www.freertos.org) whose source code may be
 * obtained from http://sourceforge.net/projects/freertos/files/ or on request.
 */

#include "main.h"
#include "drivetrain.h"
#include "intake.h"
#include "launcher.h"
void operatorControl() {
	while (1) {
		drivetrain();
		intake();
		launcher();
		delay(20);
	}
}
