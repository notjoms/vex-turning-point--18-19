#include "main.h"
#include "fourbar.h"
void fourbar(){
  if(joystickGetDigital(1,8,JOY_UP)) motorSet(motorLookup("fourBar"),127);
  else if(joystickGetDigital(1,8,JOY_DOWN)) motorSet(motorLookup("fourBar"),-127);
  else motorSet(motorLookup("fourBar"), 0);
}
