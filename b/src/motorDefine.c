#include "main.h"
#include "motorDefine.h"
const char *motorList[] = {
  "",//1
  "FLDT",//2
  "fourBar",//3
  "BLDT",//4
  "flipper",//5
  "intake",//6
  "BRDT",//7
  "launcher",//8
  "FRDT",//9
  "",//10
};
const char *analogSensorList[] = {
  "",//1
  "",//2
  "",//3
  "",//4
  "",//5
  "",//6
  "",//7
  "",//8
};
const char *digitalSensorList[] = {
  "",//1
  "",//2
  "",//3
  "",//4
  "",//5
  "",//6
  "",//7
  "",//8
  "",//9
  "",//10
  "",//11
  "",//12
};
int motorLookup(char* motorString){
  for(int i = 0; i < 10; i++){
    if(motorList[i] == motorString) return i+1;
  }
  return -1;
}
int analogSensorLookup(char* sensorString){
  for(int i = 0; i < 8; i++){
    if(analogSensorList[i] == sensorString) return i+1;
  }
  return -1;
}
int digitalSensorLookup(char* sensorString){
  for(int i = 0; i < 12; i++){
    if(digitalSensorList[i] == sensorString) return i+1;
  }
  return -1;
}
char* reverseMotorLookup(int a){
  return motorList[a];
}
char* reverseAnalogSensorLookup(int a){
  return analogSensorList[a];
}
char* reverseDigitalSensorLookup(int a){
  return digitalSensorList[a];
}
