#include "main.h"
#include "drivetrain.h"
#include "motorDefine.h"
void drivetrain(){
  motorSet((char)motorLookup("FLDT"), abs(joystickGetAnalog(1, 3) > 20 ? joystickGetAnalog(1, 3) : 0));
  motorSet((char)motorLookup("BLDT"), abs(joystickGetAnalog(1, 3) > 20 ? joystickGetAnalog(1, 3) : 0));
  motorSet((char)motorLookup("FRDT"), abs(joystickGetAnalog(1, 2) > 20 ? -joystickGetAnalog(1, 2) : 0));
  motorSet((char)motorLookup("BRDT"), abs(joystickGetAnalog(1, 2) > 20 ? -joystickGetAnalog(1, 2) : 0));
}
